const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0c271d", /* black   */
  [1] = "#40c792", /* red     */
  [2] = "#31aa7c", /* green   */
  [3] = "#33aa91", /* yellow  */
  [4] = "#2ea7a6", /* blue    */
  [5] = "#238caa", /* magenta */
  [6] = "#166aaa", /* cyan    */
  [7] = "#c2c9c6", /* white   */

  /* 8 bright colors */
  [8]  = "#485d55",  /* black   */
  [9]  = "#40c792",  /* red     */
  [10] = "#31aa7c", /* green   */
  [11] = "#33aa91", /* yellow  */
  [12] = "#2ea7a6", /* blue    */
  [13] = "#238caa", /* magenta */
  [14] = "#166aaa", /* cyan    */
  [15] = "#c2c9c6", /* white   */

  /* special colors */
  [256] = "#0c271d", /* background */
  [257] = "#c2c9c6", /* foreground */
  [258] = "#c2c9c6",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
