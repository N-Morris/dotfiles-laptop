" wal.vim -- Vim color scheme. (Modified)
" Author:       Dylan Araps
" Webpage:      https://github.com/dylanaraps/wal
" Description:  A colorscheme that uses your terminal colors, made to work with 'wal'.

hi clear
set background=dark

if exists('syntax_on')
    syntax reset
endif

let g:colors_name = 'wal'

hi Normal guibg=#0c271d guifg=#c2c9c6
hi NonText guibg=NONE guifg=#0c271d
hi Comment guibg=NONE guifg=#485d55
hi Constant guibg=NONE guifg=#33aa91
hi Error guibg=#40c792 guifg=#c2c9c6
hi Identifier guibg=NONE guifg=#40c792 gui=BOLD
hi Ignore guibg=#485d55 guifg=#0c271d
hi PreProc guibg=NONE guifg=#33aa91
hi Special guibg=NONE guifg=#166aaa
hi Statement guibg=NONE guifg=#40c792
hi String guibg=NONE guifg=#31aa7c
hi Number guibg=NONE guifg=#33aa91
hi Todo guibg=#31aa7c guifg=#0c271d
hi Type guibg=NONE guifg=#33aa91
hi Underlined guibg=NONE guifg=#40c792 gui=underline
hi StatusLine guibg=#c2c9c6 guifg=#0c271d
hi StatusLineNC guibg=NONE guifg=NONE
hi TabLine guibg=NONE guifg=#485d55
hi TabLineFill guibg=NONE guifg=#485d55
hi TabLineSel guibg=#2ea7a6 guifg=#0c271d
hi TermCursorNC guibg=#33aa91 guifg=#0c271d
hi VertSplit guibg=NONE guifg=NONE
hi Title guibg=NONE guifg=#2ea7a6
hi CursorLine guibg=#485d55 guifg=#0c271d
hi LineNr guibg=NONE guifg=#485d55
hi CursorLineNr guibg=NONE guifg=#485d55
hi helpLeadBlank guibg=NONE guifg=#c2c9c6
hi helpNormal guibg=NONE guifg=#c2c9c6
hi Visual guibg=#0c271d guifg=#c2c9c6 gui=reverse term=reverse
hi VisualNOS guibg=NONE guifg=#40c792
hi Pmenu guibg=#485d55 guifg=#c2c9c6
hi PmenuSbar guibg=#166aaa guifg=#c2c9c6
hi PmenuSel guibg=#2ea7a6 guifg=#0c271d
hi PmenuThumb guibg=#485d55 guifg=#485d55
hi FoldColumn guibg=NONE guifg=#c2c9c6
hi Folded guibg=NONE guifg=#485d55
hi WildMenu guibg=#31aa7c guifg=#0c271d
hi SpecialKey guibg=NONE guifg=#485d55
hi DiffAdd guibg=NONE guifg=#31aa7c
hi DiffChange guibg=NONE guifg=#485d55
hi DiffDelete guibg=NONE guifg=#40c792
hi DiffText guibg=NONE guifg=#2ea7a6
hi IncSearch guibg=#33aa91 guifg=#0c271d
hi Search guibg=#33aa91 guifg=#0c271d
hi Directory guibg=NONE guifg=#2ea7a6
hi MatchParen guibg=#485d55 guifg=#0c271d
hi ColorColumn guibg=#2ea7a6 guifg=#0c271d
hi signColumn guibg=NONE guifg=#2ea7a6
hi ErrorMsg guibg=NONE guifg=#485d55
hi ModeMsg guibg=NONE guifg=#31aa7c
hi MoreMsg guibg=NONE guifg=#31aa7c
hi Question guibg=NONE guifg=#2ea7a6
hi WarningMsg guibg=#40c792 guifg=#0c271d
hi Cursor guibg=#c2c9c6 guifg=#0c271d
hi Structure guibg=NONE guifg=#238caa
hi CursorColumn guibg=#485d55 guifg=#c2c9c6
hi ModeMsg guibg=NONE guifg=#c2c9c6
hi SpellBad guibg=NONE guifg=#40c792 gui=underline
hi SpellCap guibg=NONE guifg=#2ea7a6 gui=underline
hi SpellLocal guibg=NONE guifg=#238caa gui=underline
hi SpellRare guibg=NONE guifg=#166aaa gui=underline
hi Boolean guibg=NONE guifg=#238caa
hi Character guibg=NONE guifg=#40c792
hi Conditional guibg=NONE guifg=#238caa
hi Define guibg=NONE guifg=#238caa
hi Delimiter guibg=NONE guifg=#238caa
hi Float guibg=NONE guifg=#238caa
hi Include guibg=NONE guifg=#2ea7a6
hi Keyword guibg=NONE guifg=#238caa
hi Label guibg=NONE guifg=#33aa91
hi Operator guibg=NONE guifg=#c2c9c6
hi Repeat guibg=NONE guifg=#33aa91
hi SpecialChar guibg=NONE guifg=#238caa
hi Tag guibg=NONE guifg=#33aa91
hi Typedef guibg=NONE guifg=#33aa91
hi vimUserCommand guibg=NONE guifg=#40c792 gui=BOLD
    hi link vimMap vimUserCommand
    hi link vimLet vimUserCommand
    hi link vimCommand vimUserCommand
    hi link vimFTCmd vimUserCommand
    hi link vimAutoCmd vimUserCommand
    hi link vimNotFunc vimUserCommand
hi vimNotation guibg=NONE guifg=#2ea7a6
hi vimMapModKey guibg=NONE guifg=#2ea7a6
hi vimBracket guibg=NONE guifg=#c2c9c6
hi vimCommentString guibg=NONE guifg=#485d55
hi htmlLink guibg=NONE guifg=#40c792 gui=underline
hi htmlBold guibg=NONE guifg=#33aa91 gui=BOLD
hi htmlItalic guibg=NONE guifg=#238caa
hi htmlEndTag guibg=NONE guifg=#c2c9c6
hi htmlTag guibg=NONE guifg=#c2c9c6
hi htmlTagName guibg=NONE guifg=#40c792 gui=BOLD
hi htmlH1 guibg=NONE guifg=#c2c9c6
    hi link htmlH2 htmlH1
    hi link htmlH3 htmlH1
    hi link htmlH4 htmlH1
    hi link htmlH5 htmlH1
    hi link htmlH6 htmlH1
hi cssMultiColumnAttr guibg=NONE guifg=#31aa7c
    hi link cssFontAttr cssMultiColumnAttr
    hi link cssFlexibleBoxAttr cssMultiColumnAttr
hi cssBraces guibg=NONE guifg=#c2c9c6
    hi link cssAttrComma cssBraces
hi cssValueLength guibg=NONE guifg=#c2c9c6
hi cssUnitDecorators guibg=NONE guifg=#c2c9c6
hi cssValueNumber guibg=NONE guifg=#c2c9c6
    hi link cssValueLength cssValueNumber
hi cssNoise guibg=NONE guifg=#485d55
hi cssTagName guibg=NONE guifg=#40c792
hi cssFunctionName guibg=NONE guifg=#2ea7a6
hi scssSelectorChar guibg=NONE guifg=#c2c9c6
hi scssAttribute guibg=NONE guifg=#c2c9c6
    hi link scssDefinition cssNoise
hi sassidChar guibg=NONE guifg=#40c792
hi sassClassChar guibg=NONE guifg=#238caa
hi sassInclude guibg=NONE guifg=#238caa
hi sassMixing guibg=NONE guifg=#238caa
hi sassMixinName guibg=NONE guifg=#2ea7a6
hi javaScript guibg=NONE guifg=#c2c9c6
hi javaScriptBraces guibg=NONE guifg=#c2c9c6
hi javaScriptNumber guibg=NONE guifg=#238caa
hi markdownH1 guibg=NONE guifg=#c2c9c6
    hi link markdownH2 markdownH1
    hi link markdownH3 markdownH1
    hi link markdownH4 markdownH1
    hi link markdownH5 markdownH1
    hi link markdownH6 markdownH1
hi markdownAutomaticLink guibg=NONE guifg=#31aa7c gui=underline
    hi link markdownUrl markdownAutomaticLink
hi markdownError guibg=NONE guifg=#c2c9c6
hi markdownCode guibg=NONE guifg=#33aa91
hi markdownCodeBlock guibg=NONE guifg=#33aa91
hi markdownCodeDelimiter guibg=NONE guifg=#238caa
hi markdownItalic gui=Italic
hi markdownBold gui=Bold
hi xdefaultsValue guibg=NONE guifg=#c2c9c6
hi rubyInclude guibg=NONE guifg=#2ea7a6
hi rubyDefine guibg=NONE guifg=#238caa
hi rubyFunction guibg=NONE guifg=#2ea7a6
hi rubyStringDelimiter guibg=NONE guifg=#31aa7c
hi rubyInteger guibg=NONE guifg=#33aa91
hi rubyAttribute guibg=NONE guifg=#2ea7a6
hi rubyConstant guibg=NONE guifg=#33aa91
hi rubyInterpolation guibg=NONE guifg=#31aa7c
hi rubyInterpolationDelimiter guibg=NONE guifg=#33aa91
hi rubyRegexp guibg=NONE guifg=#166aaa
hi rubySymbol guibg=NONE guifg=#31aa7c
hi rubyTodo guibg=NONE guifg=#485d55
hi rubyRegexpAnchor guibg=NONE guifg=#c2c9c6
    hi link rubyRegexpQuantifier rubyRegexpAnchor
hi pythonOperator guibg=NONE guifg=#238caa
hi pythonFunction guibg=NONE guifg=#2ea7a6
hi pythonRepeat guibg=NONE guifg=#238caa
hi pythonStatement guibg=NONE guifg=#40c792 gui=Bold
hi pythonBuiltIn guibg=NONE guifg=#2ea7a6
hi phpMemberSelector guibg=NONE guifg=#c2c9c6
hi phpComparison guibg=NONE guifg=#c2c9c6
hi phpParent guibg=NONE guifg=#c2c9c6
hi cOperator guibg=NONE guifg=#166aaa
hi cPreCondit guibg=NONE guifg=#238caa
hi SignifySignAdd guibg=NONE guifg=#31aa7c
hi SignifySignChange guibg=NONE guifg=#2ea7a6
hi SignifySignDelete guibg=NONE guifg=#40c792
hi NERDTreeDirSlash guibg=NONE guifg=#2ea7a6
hi NERDTreeExecFile guibg=NONE guifg=#c2c9c6
hi ALEErrorSign guibg=NONE guifg=#40c792
hi ALEWarningSign guibg=NONE guifg=#33aa91
hi ALEError guibg=NONE guifg=#40c792
hi ALEWarning guibg=NONE guifg=#33aa91

let g:limelight_conceal_guifg = 8
