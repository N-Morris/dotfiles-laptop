set nocompatible

source $VIMRUNTIME/vimrc_example.vim
set background=dark
colorscheme wal
set lines=999 columns=999
set number
set noswapfile
set nobackup
map <F7> :tabp<CR>
map <F9> :tabn<CR>
map <F8> :tabe
set tabstop=4
set shiftwidth=4
set softtabstop=0 noexpandtab
set noexpandtab
set noundofile
set list lcs=tab:\│\ 
set completeopt-=preview
set guifont=Source\ Code\ Pro\ for\ Powerline\ 9
set go-=m
set go-=T
set go-=r
set go-=e
set showtabline=2
let g:powerline_pycmd="py3"
set laststatus=2
set noshowmode
cmap w!! w !sudo tee > /dev/null %
map J :tabn<CR>
map K :tabp<CR>
source $VIMRUNTIME/mswin.vim
behave mswin
syntax enable
